package com.punicapp.google;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.punicapp.sn.SnGoogle;
import com.punicapp.sn.google.GoogleSNConnector;
import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNManager;

import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.ResourceSingleObserver;

public class MainActivity extends AppCompatActivity {

    private SNManager sManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SNManager.Builder builder = new SNManager.Builder(this);
        sManager = builder
                .with(new GoogleSNConnector("811849978200-9ebr6sr8o2k93tqem0ar97oer21qlr2d.apps.googleusercontent.com"))
                .build();

        findViewById(R.id.login_google).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Single<SNAccessToken> auth = sManager.auth(SnGoogle.TYPE);
                auth.subscribe(new ResourceSingleObserver<SNAccessToken>() {
                    @Override
                    public void onSuccess(SNAccessToken snAccessToken) {
                        showDialog(snAccessToken.toString());
                        dispose();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        showDialog("ERRROR: " + e.getMessage());
                        dispose();
                    }
                });
            }
        });


    }

    private void showDialog(String message) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}

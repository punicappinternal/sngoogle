package com.punicapp.sn.google;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.punicapp.sn.SnGoogle;
import com.punicapp.sncore.AuthCanceledException;
import com.punicapp.sncore.ISNConnector;
import com.punicapp.sncore.SNAccessToken;

import io.reactivex.SingleObserver;
import io.reactivex.subjects.SingleSubject;

public class GoogleSNConnector implements ISNConnector {
    private static final int SIGN_IN_REQUEST_CODE = 0x60061E;
    private SingleObserver<SNAccessToken> authObserver;
    private SingleObserver<Boolean> logoutObserver;
    private String clientId;

    public GoogleSNConnector(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String getType() {
        return SnGoogle.TYPE;
    }

    @Override
    public boolean requestAuth(Activity shadowActivity) {
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(shadowActivity, getGoogleSignInOptions());
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(shadowActivity);
        if (account != null) {
            returnData(account);
            return true;
        }

        Intent signInIntent = googleSignInClient.getSignInIntent();
        shadowActivity.startActivityForResult(signInIntent, SIGN_IN_REQUEST_CODE);

        return false;
    }

    @Override
    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGN_IN_REQUEST_CODE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);

                returnData(account);
            } catch (ApiException e) {
                Exception err = e.getStatusCode() == GoogleSignInStatusCodes.SIGN_IN_CANCELLED
                        ? new AuthCanceledException() : e;
                authObserver.onError(err);
            }
        }
    }

    @Override
    public void bindAuthCallback(SingleSubject<SNAccessToken> observer) {
        this.authObserver = observer;
    }

    @Override
    public void bindLogoutCallback(SingleSubject<Boolean> observer) {
        this.logoutObserver = observer;
    }

    private void returnData(GoogleSignInAccount account) {
        authObserver.onSuccess(new SNAccessToken(account.getIdToken(), null, account.getId()));
    }

    @Override
    public void logout(Context context) {
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(context, getGoogleSignInOptions());
        googleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                logoutObserver.onSuccess(true);
            }
        });
    }

    private GoogleSignInOptions getGoogleSignInOptions() {
        return new GoogleSignInOptions.Builder()
                .requestIdToken(clientId)
                .requestId()
                .requestEmail()
                .build();
    }
}
